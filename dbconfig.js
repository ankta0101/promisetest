'use strict';

const dbconfig = require('./dbconfig.json');

module.exports = {

    host : dbconfig.host,
    user: dbconfig.user,
    password: dbconfig.password,
    database: dbconfig.database,
    port:dbconfig.port
    // host : "localhost",
    // user: "root",
    // password: "root",
    // database: "testdb"
};