module.exports = function (conn, query, values) {
    return new Promise(function (resolve, reject) {
        conn.query(query, values, function (err, rows, fields) {
            if (err) {
                console.log('rejecting ', query, values);
                console.log(err);
                reject(err);
            }
            else {
                resolve({ rows: rows, fields: fields });
            }
        });
    });
}