"use strict";
const execQuery = require("./execQuery");
const MySQL = require("mysql");
const dbconfig = require("./dbconfig");
const request = require('request');
var conn = MySQL.createConnection(dbconfig);
var config = require("./config")
var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var date = require('date-fns')
const path = require('path');
const fs = require('fs');
var moment = require('moment');
app.use(bodyParser.json());
let XLSX = require('xlsx');
let Client = require('ssh2').Client;
let con = new Client();
//let app2 = require('./app2')


//sftpconfig();
 function sftpconfig() {
    return new Promise(function (resolve, reject) {
        con.on('ready', function () {
            con.sftp(function (err, sftp) {
                if (err) throw err;
                sftp.readdir(config.FileReadPath, function (err, list) {
                    if (err) throw err;

                    // List the directory in the console

                    console.dir(list);
                    for (let records of list) {
                        console.log("list", records.filename);
                        let name = records.filename;
                        let innerdate = name.split('_')[3] !== undefined ? name.split('_')[3].substring(0, name.split('_')[3].indexOf('.xlsx')) : false
                        console.log("innerdate", innerdate)
                        if (date.isPast(date.parse(innerdate, 'dd-LL-yyyy-HH-mm', new Date()), new Date()) && name.split('_')[0] === 'Dispatch' && config.Courier.value.includes(name.split('_')[1]) && config.Tier.value.includes(name.split('_')[2]) || date.isSameDay(date.parse(innerdate, 'dd-LL-yyyy-HH-mm', new Date()), new Date()) && name.split('_')[0] === 'Dispatch' && config.Courier.value.includes(name.split('_')[1]) && config.Tier.value.includes(name.split('_')[2])) {
                            console.log('file after filter', name);
                            var moveFrom = config.FileReadPath + '/' + records.filename;
                            var moveTo = config.ReadPath + '/' + records.filename;
                            sftp.fastGet(moveFrom, moveTo, {}, function (downloadError) {
                                if (downloadError) throw downloadError;
                                console.log("Succesfully uploaded");
                                let Rename = name.substring(0, name.indexOf('.xlsx'));
                                let RenameFile = Rename + "_done";
                                sftp.rename(config.FileReadPath + "/" + name, config.FileReadPath + "/" + RenameFile + ".xlsx", function (err) {
                                    console.log("rename done on sftp")
                                    con.end();
                                })
                                filePathRead(name);


                                // readXlsxFile(name);

                                //  console.log("connection end", con.end());
                            });

                        }
                    }

                });

            });
        }).connect(config.sftp_connection);
    })
}
function filePathRead() {
    return new Promise(function (resolve, reject) {

        // console.log("sftp file", filename);
        const directoryPath = path.join(__dirname, config.ReadPath);

        fs.readdir(directoryPath, function (err, files) {
            //handling error
            if (err) {
                return console.log('Unable to scan directory: ' + err);
            }
            //listing all files using forEach
            let currentArray = [];
            files.forEach(function (file) {

                readXlsxFile(file);

            })

        })
    })
}

function readXlsxFile(file) {
    return new Promise(function (resolve, reject) {
        let workbook = XLSX.readFile(config.ReadPath + '/' + file);
        let sheet_name_list = workbook.SheetNames;
        let result = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
        console.log("xlsx reading file data", result);

        fs.readdir(config.ReadPath, function (err, files) {
            if (err) throw err;
            files.forEach(function (file) {
                const directoryPathFile = path.join(__dirname, config.ReadPath);
                const directoryPathRename = path.join(__dirname,config.RenamePath);

                let Rename = file.substring(0, file.indexOf('.xlsx'));
                //console.log("Rename",Rename);
                let RenameFile = Rename + "_done";
                //console.log("After Rename",RenameFile);
                fs.rename(directoryPathFile + "/" + file, directoryPathRename + "/" + RenameFile + ".xlsx", function (err) {
                    if (err) {
                        console.log("error")
                    };
                    console.log("Rename done===========================");
                });
            })
        })

        insertDispatchTable(result, file);

    })
}

function insertDispatchTable(data1, file) {
    return new Promise((resolve, reject) => {
        //  console.log("File Name========", file);
        //  console.log("Data in Insert Dispatch table function------------", data1);
        var newarr = [];
        let d_success = 0;
        let d_failed = 0;
        let d_total = 0;

        newarr = [file, d_total, d_success, d_failed];
        var query = "INSERT INTO `trn_dispatches` (dp_filename,dp_recordcount,dp_successcount,dp_failedcount) VALUES "
            + " (?,?,?,?) "
        console.log("query1==", query);
        execQuery(conn, query, newarr)
            .then((result) => {
                // renameFile(file);
                // console.log("inserted dispatch table function", result);
                validation(data1, result, d_total, d_success, d_failed, file);

            })
    })
        .catch((error) => {
            console.log("----Error----------", error);

            resolve();
        })
}


function validation(data3, result, d_total, d_success, d_failed, file) {
    for (let i = 0; i < data3.length; i++) {
        // console.log("validation data", JSON.stringify(data3[i]).split(' ').join('_'));
        // console.log("=", JSON.stringify(data3[i]).split('(DD-MM-YYYY)').join(''))
        let datejson = JSON.stringify(data3[i]).split('(DD-MM-YYYY)').join('');
        let datejson1 = JSON.parse(datejson);
       // console.log("datejson1====", datejson1)
        let data = JSON.stringify(datejson1).split(' ').join('_');
        //console.log("data======", data);
        let Datas = JSON.parse(data);
        //console.log("jpno========", Datas)
        let status = ""
        let error = ""

        var Encode = true;
        var flag = true;


        var dispatchquery = "Select * from trn_dispatchdetails where dpd_awb = '" + Datas.AWB + "' AND dpd_status = 'success'"
        execQuery(conn, dispatchquery)
            .then((res1) => {
                // console.log("Dispatch response =====", res1.rows);
                //console.log(data3[i].AWB, "awb response============================")
                if (res1.rows && res1.rows.length > 0) {
                    res1.rows.forEach((row) => {
                        console.log("row===========", row.dpd_awb);
                        if (row.dpd_awb == Datas.AWB) {
                            flag = false;
                            error = "Same AWB Number has been already assigned to different InterMiles Membership Number";
                            status = "Failed"
                        }
                        // console.log("error status", error, status, flag);
                    })
                    // console.log("error status==", error, status, flag);
                }
                // console.log("error status=====", error, status, flag);
                let jpno = ""
                jpno = Datas.InterMiles_Membership_Number
                if (!jpno == "") {
                    let jp = Number(Datas.InterMiles_Membership_Number);
                    let digit = parseInt(jp % 10);
                    let mod = parseInt((jp / 10) % 7);
                    // console.log("jp digit mod", jpno, jp, digit, mod)
                    if (digit == mod || jp == 0) {
                        // if (flag == false) {
                        //   status = "Failed"
                        // }
                        status = "success"
                    }
                    else {
                        if (flag == false) {
                            status = "Failed"
                        }
                        status = "Failed"
                        error = "Invalid InterMiles Membership Number"
                        flag = false;
                    }
                }
                else {
                    jpno = ""
                    status = "Failed"
                    flag = false;
                    error = "empty InterMiles Membership Number"
                    if (flag == false) {
                        status = "Failed"
                    }
                }


                //Tier code validation
                let tier = ""
                tier = Datas.Tier_Code;
                // console.log("===================================tier", tier, config.Tier.value, config.Tier.value.indexOf(tier) > -1)

                //if (!config.Tier.value.indexOf(tier) > -1)
                if (tier == "Red" || tier == "Silver" || tier == "Gold" || tier == "Platinum") {
                    status = "success"
                    Encode = true
                    // if (flag == false) {
                    //   status = "Failed"
                    // }
                } else {

                    if (flag == false) {
                        status = "Failed"
                        error += error + ",";
                    }
                    status = "Failed"
                    error += "Invalid tier code,"
                    flag = false
                }
                // AWB validation
                let awb = ""
                awb = Datas.AWB
                console.log("validation awb ========================================", awb)
                if (!awb == "") {
                    status = "success"
                }
                else {
                    awb == ""
                    status = "Failed"
                    error = "Invalid Awb Number"
                    flag = false

                }


                // Courier validation
                let courier = ""
                courier = Datas.Courier_Partner_Name;
                // console.log("===================================courier", courier, config.Courier.value, config.Courier.value.indexOf(courier) > -1)

                //if (!config.Courier.value.indexOf(courier) > -1)
                if (courier == "Trackon" || courier == "DHL" || courier == "Indiapost" || courier == "FirstFlight" || courier == "Overnite") {
                    status = "success"
                    Encode = true
                    if (flag == false) {
                        status = "Failed"
                    }
                } else {
                    courier = ""
                    status = "failed"
                    error += "Invalid courier name , "
                    flag = false
                    if (flag == false) {
                        status = "Failed"
                    }
                }

                //Enhancment validation
                let Enhancement = "";
                let tc = "";
                let cou = "";
                tc = Datas.Tier_Code;
                cou = Datas.Courier_Partner_Name;
                if (Encode = true) {
                    console.log("Tiercode" + tc + "Courier Name" + cou);
                    if (tc == ("GOLD") && (cou == ("Overnite") || cou == ("DHL")) || cou == ("Trackon")) {
                        Enhancement = "JPWG_C";
                    }
                    if (tc == ("GOLD") && (cou == ("Indiapost"))) {
                        Enhancement = "JPWG_P";
                    }
                    if (tc == ("SILVER") && (cou == ("Overnite") || cou == ("DHL")) || cou == ("Trackon")) {
                        Enhancement = "JPWS_C";
                    }
                    if (tc == ("SILVER") && (cou == ("Indiapost"))) {
                        Enhancement = "JPWS_P";
                    }
                    if (tc == ("PLATINUM") && (cou == ("Overnite") || cou == ("DHL")) || cou == ("Trackon")) {
                        Enhancement = "JPWP_C";
                    }
                    if (tc == ("PLATINUM") && (cou == ("Indiapost"))) {
                        Enhancement = "JPWP_P";
                    }
                    if (tc == ("RED") && (cou == ("Overnite") || cou == ("DHL")) || cou == ("Trackon")) {
                        Enhancement = "JPWB_C";
                    }
                    if (tc == ("RED") && (cou == ("Indiapost"))) {
                        Enhancement = "JPWB_P";
                    }
                } else {
                    Enhancement = "";
                }

                //cardpull date validation
                var carddate = ""
                var cardpulldate = ""
                var date1 = new Date();
                date1 = null
                try {
                    cardpulldate = Datas.CardPull_Date_;

                    // console.log(cardpulldate.length, "data---------------")
                    // console.log(cardpulldate.charAt(2), "data--index-------------")

                    var ch = cardpulldate.charAt(2)
                    if (cardpulldate.length == 10) {
                        if (ch == '-') {
                            date1 = moment(cardpulldate, 'DD-MM-YYYY')
                            // date1= date1._i
                            carddate = date1.format('YYYY-MM-DD')
                            flag = true

                        } else {
                            carddate = ""
                            flag = false
                            status = "Failed"
                            error = "Invalid date formate"
                        }
                    } else {

                        carddate = data3[i].CardPull_Date
                        flag = false
                        status = "Failed"
                        error = "Invalid date formate"
                    }
                }
                catch (e) {

                    cardpulldate = ""
                    status = "Failed"
                    error = "cardpull date is Missing"
                    flag = false
                }

                var resultdate = ""
                var dispatchdate = ""
                var date2 = new Date();
                if (!data3[i].Dispatch_Date == "") {
                    try {
                        dispatchdate = Datas.Dispatch_Date;

                        // console.log(dispatchdate.length, "data---------------")
                        // console.log(dispatchdate.charAt(2), "data--index-------------")

                        var dh = dispatchdate.charAt(2)
                        if (dispatchdate.length == 10) {
                            if (dh == '-') {
                                date2 = moment(dispatchdate, 'DD-MM-YYYY')

                                resultdate = date2.format('YYYY-MM-DD')
                                //console.log("date2----", date2._i)
                                flag = true;

                                // console.log("date2------------", resultdate)
                                // console.log("Date1 and date2", date1._i, date2._i, date2._i < date1._i,resultdate < carddate)
                                if (resultdate < carddate) {
                                    console.log("Date1 resultdate and carddatedate", resultdate < carddate)
                                    // flag = false
                                    status = "Failed"
                                    error = "Cardpull Date must be less than or equal to Dispatch date"
                                    // if (flag = false) {
                                    //   status = "Failed"
                                    // }
                                }

                            } else {

                                resultdate = ""
                                flag = false
                                status = "Failed"
                                error = "Invalid dispatch date formate"
                            }
                        } else {

                            resultdate = data3[i].Dispatch_Date
                            flag = false
                            status = "Failed"
                            error = "Invalid dispatch date formate"

                        }
                    }
                    catch (e) {
                        resultdate = ""
                        status = "Failed"
                        error = "dispatch date is Missing"
                        flag = false
                    }
                }


                if (status == "success") {
                    d_success++;

                } else {
                    d_failed++
                }
                insertDispatchDetailsTable(Datas, result.rows.insertId, status, error, Enhancement, flag, d_total, d_success, d_failed, file)
                    .then((res) => {
                        console.log("value in insert dispatch response", res);
                        resolve(res);
                    });
                
            })
    }
}

function insertDispatchDetailsTable(data2, id, status, error, Enhancement, flag, d_total, d_success, d_failed, file) {
    return new Promise((resolve, reject) => {
      //  console.log("dispatch details table data====", data2);
      //  console.log("dispatch details table flag, status, error", flag, status, error)
        // console.log("id", id);
        let dpd_error_description = error
        let cr_code = config.CRCODE
        let en_code = Enhancement
        let stat = status
        var newarr1 = [];
        newarr1 = [id, data2.InterMiles_Membership_Number, data2.Tier_Code, data2.Courier_Partner_Name, data2.AWB, data2.Dispatch_Date, cr_code, en_code, stat, dpd_error_description, data2.CardPull_Date_];
        var query = "INSERT INTO `trn_dispatchdetails` (dpd_dp_id,dpd_jpno,dpd_tiercode,dpd_courier,dpd_awb,dpd_dispatchdate,dpd_cr_code,dpd_en_code,dpd_status,dpd_error_description,dpd_cardpulldate) VALUES "
            + " (?,?,?,?,?,?,?,?,?,?,?) "
        console.log("trn_dispatchdetails table query", query);
        execQuery(conn, query, newarr1)
            .then((result) => {
                // console.log("inserted dispatchdetails table function", result);

                 updateDispatchTable(id, flag, d_success, d_failed, d_total, file);

                if (stat == 'success') {
                    memberProfile(data2, stat)
                }

            })
    })
        .catch((error) => {
            console.log("----Error----------", error);
            resolve();
        })
}

function updateDispatchTable(id, flag, d_success, d_failed, d_total, file) {
    return new Promise(function (resolve, reject) {
        d_total = d_success + d_failed;
        var query = "UPDATE trn_dispatches SET dp_recordcount=" + d_total + ",dp_successcount=" + d_success + ",dp_failedcount=" + d_failed + " WHERE dp_id=" + id;
        console.log("update query Dispatch Table query", query);
        execQuery(conn, query)
            .then((result) => {
                //console.log("Updated Dispatch Table status", result);
              // getTotalData(id, file);
            })
    })

}


function getTotalData(id, file) {

    var query = "select dpd_jpno as InterMiles_Membership_Number,dpd_tiercode as Tier,dpd_awb as AWB,dpd_courier as Courier_Partner_Name,dpd_status as Status,dpd_en_code as Status_Enhancment_Code,dpd_cr_code as CR_Code,dpd_cardpulldate as Cardpull_Date,dpd_dispatchdate as Dispatch_Date,dpd_error_description as Error_Description from trn_dispatchdetails where dpd_dp_id=" + id;
    //console.log("getTotalData query", query)
    var arr = []
    execQuery(conn, query)

        .then((resp) => {
            //   if (resp.rows && resp.rows.length >= 0) {
            resp.rows.forEach((row) => {
                //  console.log("row getTotalData", row.Status);
                  arr.push(row);

                const directoryPath = path.join(__dirname, config.Total);
                let filename = file.substring(0, file.indexOf('.xlsx'));
                let filename1 = filename + '_Totalcount';

                var ws = XLSX.utils.json_to_sheet(arr);
              var wb = XLSX.utils.book_new();
              XLSX.utils.book_append_sheet(wb, ws);
              XLSX.writeFile(wb, directoryPath + "/" + filename1 + ".xlsx")
              console.log("total file==================",filename1)
              //conn.end();
            });
        })

    var squery = "select dpd_jpno as InterMiles_Membership_Number,dpd_tiercode as Tier,dpd_awb as AWB,dpd_courier as Courier_Partner_Name,dpd_status as Status,dpd_en_code as Status_Enhancment_Code,dpd_cr_code as CR_Code,dpd_cardpulldate as Cardpull_Date,dpd_dispatchdate as Dispatch_Date,dpd_error_description as Error_Description from trn_dispatchdetails where dpd_status='success' AND dpd_dp_id=" + id;
    // console.log("getSuccessData query ", squery)
      var sarr = []
    execQuery(conn, squery)

        .then((resp) => {
            //  if (resp.rows && resp.rows.length >= 0) {
            resp.rows.forEach((row) => {
                //    console.log("row getSuccessData", row.Status);
                  sarr.push(row);

                const directoryPath = path.join(__dirname, config.Success);
                let filename = file.substring(0, file.indexOf('.xlsx'));
                let filename1 = filename + '_Successcount';
                var ws = XLSX.utils.json_to_sheet(sarr);
              var wb = XLSX.utils.book_new();
              XLSX.utils.book_append_sheet(wb, ws);
              XLSX.writeFile(wb, directoryPath + "/" + filename1 + ".xlsx")
              console.log("success file==================",filename1)
            //  conn.end();
            
            });
        })


    var fquery = "select dpd_jpno as InterMiles_Membership_Number,dpd_tiercode as Tier,dpd_awb as AWB,dpd_courier as Courier_Partner_Name,dpd_status as Status,dpd_en_code as Status_Enhancment_Code,dpd_cr_code as CR_Code,dpd_cardpulldate as Cardpull_Date,dpd_dispatchdate as Dispatch_Date,dpd_error_description as Error_Description from trn_dispatchdetails where dpd_status='Failed' AND dpd_dp_id=" + id;
   //console.log("getFailedData query", fquery)
        var farr = []
    execQuery(conn, fquery)
        .then((resp) => {
            //  if (resp.rows && resp.rows.length >= 0) {
            resp.rows.forEach((row) => {
                // console.log("row getFailedData", row.Status);
                   farr.push(row);

                const directoryPath = path.join(__dirname, config.Failed);
                let filename = file.substring(0, file.indexOf('.xlsx'));
                let filename1 = filename + '_FailedCount';
                var ws = XLSX.utils.json_to_sheet(farr);
              var wb = XLSX.utils.book_new();
              XLSX.utils.book_append_sheet(wb, ws);
              XLSX.writeFile(wb, directoryPath + "/" + filename1 + ".xlsx")
              console.log("failed file==================",filename1)
             // conn.end();
            });
        })
    
}


function memberProfile(datam, status) {
    return new Promise((resolve, reject) => {
  //      console.log("Member profile function data=======", datam)
        // console.log("Memebr status", status)
        let jp_number = datam.InterMiles_Membership_Number;
        // console.log("member jp", jp_number);

        let name = "bdPXxLAhxTf7yYuFe9E8k0esoLmkuIyC";
        let password = "RHJpZSpsMNWBnw6Z2qMeOZCXExfl9NJU";

        let authString = name + ":" + password;
        let buff = new Buffer(authString);
        let a = buff.toString('base64');
      //  console.log('"' + authString + '" converted to Base64 is "' + a + '"');
        request.get({
            "headers": {
                "content-type": "application/json",
                'Authorization': 'Basic' + a,
                'partnerID': 'JPOPS'
            },
            "url": 'https://apiuat.jetprivilege.com:8082/api/experience/member-profile/v1/fetch-profile?JPNumber=' + jp_number,
            //"body": JSON.stringify(request)
        }, (error, response, body) => {
            if (error) {
                resolve("failed");
            } else {
                resolve(response);
                // console.log("profile================", response.body);
                sendSMS(datam, response.body, status);
                sendEmail(datam,response.body,status);

            }
        });
    });

}

function sendSMS(data1, data, status) {
    return new Promise((resolve, reject) => {
        // console.log(" data1", data1);
        // console.log("sms data", data);
        // console.log("Sms status", status);
        let tiercode = data1.Tier_Code;
        let awb = data1.AWB;
        let Courier = data1.Courier_Partner_Name
        console.log("Sms data===========", awb, tiercode, Courier)
        let temp = JSON.parse(data);
        //console.log("temp data", temp);
        let Firstname = temp.FirstName;
      //  console.log("Firstname", Firstname);
        let Mobile = temp.MobileNumber;
      //  console.log("Mobile number", Mobile);

        var dt = new Date();
        var dt1 = dt.toLocaleDateString();
        console.log(dt1);
        var sms = "Hi " + Firstname + " , your InterMiles " + tiercode + " Welcome Pack has been dispatched through " + Courier + " and it shall reach you in the next 8 to 10 days. Track your package with AWB number " + awb + " : bit.ly/2GBnoAR";

        // var url = 'https://api.mgage.solutions/SendSMS/sendmsg.php?uname=ccjetapi&pass=b~7Qn$4X&send=INTMLS&dest=' + Mobile + '&msg=' + sms;
        var url = config.ics_sms.url + "?authtoken=" + config.ics_sms.auth_token + "&to=" + Mobile + "&from=INTMLS&SMSMSGID=" + config.ics_sms.SMSMSGID + "|Dispatch|" + awb + "&text=" + sms;
        console.log("sms url", url);
        request({
            url: url,
            headers: {
                "method": 'GET',
                'Authorization': 'Basic ' + Buffer.from(config.bespoke_simplica.username + ':' + config.bespoke_simplica.password).toString('base64')
            }}, (error, response, body) => {
            if (error) {
                resolve("failed");
                console.log("failed");
            } else {
                resolve(response.body);
                console.log("success");
                console.log("===========responsesms ", response.body);

            }

        });
    });
}
async function sendEmail1(data1, response, status) {
    let tiercode = data1.Tier_Code;
  
    let awb = data1.AWB;
    let Courier = data1.Courier_Partner_Name
    let listid = null;
    if (data1.Courier_Partner_Name == 'FirstFlight') {
      listid = 58479552
    }
    else if (data1.Courier_Partner_Name == 'Overnite') {
      listid = 58498506
    }
    else if (data1.Courier_Partner_Name == 'Indiapost') {
      listid = 58482999
    }
    else if (data1.Courier_Partner_Name == 'Trackon') {
      listid = 59017867
    }
    let jpno = data1.InterMiles_Membership_Number
    //console.log("Email data===========", awb, tiercode, Courier)
    let temp = JSON.parse(response);
    //console.log("temp data", temp);
    let Title = temp.Title;
    //console.log("Title", Title);
    let Firstname = temp.FirstName;
    //console.log("Firstname", Firstname);
    let Lastname = temp.LastName;
    //console.log("Firstname", Lastname);
    let Email = temp.EmailAddress;
    //  console.log("Email id", Email)
  
    let reString = "<XTMAILING>\n"
                  + "   <CAMPAIGN_ID>"+listid+"</CAMPAIGN_ID>\n"
                  + "   <TRANSACTION_ID />\n"
                  + "   <SHOW_ALL_SEND_DETAIL>true</SHOW_ALL_SEND_DETAIL>\n"
                  + "   <SAVE_COLUMNS>\n"
                  + "       <COLUMN_NAME>Email</COLUMN_NAME>\n"
                  + "       <COLUMN_NAME>Title</COLUMN_NAME>\n"
                  + "       <COLUMN_NAME>Last_Name</COLUMN_NAME>\n"
                  + "       <COLUMN_NAME>Tier</COLUMN_NAME>\n"
                  + "       <COLUMN_NAME>AWB_NUMBER</COLUMN_NAME>\n"
                  + "       <COLUMN_NAME>Courier_Vendor_Name</COLUMN_NAME>\n"
                  + "       <COLUMN_NAME>JP_NUMBER</COLUMN_NAME>\n"
                  + "   </SAVE_COLUMNS>\n"
                  + "   <RECIPIENT>\n"
                  + "      <EMAIL>"+Email+"</EMAIL>\n"
                  + "      <BODY_TYPE>HTML</BODY_TYPE>\n"
                  + "      <PERSONALIZATION>\n"
                  + "         <TAG_NAME>Title</TAG_NAME>\n"
                  + "         <VALUE>"+Title+"</VALUE>\n"
                  + "      </PERSONALIZATION>\n"
                  + "      <PERSONALIZATION>\n"
                  + "         <TAG_NAME>Last_Name</TAG_NAME>\n"
                  + "         <VALUE>"+Lastname+"</VALUE>\n"
                  + "      </PERSONALIZATION>\n"
                  + "      <PERSONALIZATION>\n"
                  + "         <TAG_NAME>Tier</TAG_NAME>\n"
                  + "         <VALUE>"+tiercode+"</VALUE>\n"
                  + "      </PERSONALIZATION>\n"
                  + "      <PERSONALIZATION>\n"
                  + "         <TAG_NAME>AWB_NUMBER</TAG_NAME>\n"
                  + "         <VALUE>"+awb+"</VALUE>\n"
                  + "      </PERSONALIZATION>\n"
                  + "      <PERSONALIZATION>\n"
                  + "         <TAG_NAME>Courier_Vendor_Name</TAG_NAME>\n"
                  + "         <VALUE>"+Courier+"</VALUE>\n"
                  + "      </PERSONALIZATION>\n"
                  + "      <PERSONALIZATION>\n"
                  + "         <TAG_NAME>JP_NUMBER</TAG_NAME>\n"
                  + "         <VALUE>"+jpno+"</VALUE>\n"
                  + "      </PERSONALIZATION>\n"
  //                + "      <SCRIPT_CONTEXT><![CDATA[{\"items\":[{\"EXPCTD_DATE_OF_CREDIT\":\"01/08/2018\",\"JPMILESEARNED\":\"100\",\"PARTNERNAME\":                \"Snapdeal\",\"PRODUCTAMOUNT\":\"1000\",\"PRODUCTNAME\":\"earphone\"},\n"
  //                + "\n"
  //                + "                                {\"EXPCTD_DATE_OF_CREDIT\":\"11/07/18\",\"JPMILESEARNED\":\"10\",\"PARTNERNAME\":\"Ama                zon\",\"PRODUCTAMOUNT\":\"1000\",\"PRODUCTNAME\":\"powerbank\"}]}]]></SCRIPT_CONTEXT>\n"
                  + "   </RECIPIENT>\n"
                  + "</XTMAILING>";
  
                  let name = "MaousPQSBr7AH0WLi8Cgx6KIz12RQF4q";
                  let password = "AsvvAf1hf08kpYUXsdFWTbeRx8LBImw4";
                 
                  let authString = name + ":" + password;
                  let buff = new Buffer(authString);
                  let a = buff.toString('base64');
                  console.log('"' + authString + '" converted to Base64 is "' + a + '"');
                  request.post({
                    "headers": {
                      "content-type": "application/xml",
                      'Authorization': 'Basic' + a,
                      'partnerID': 'Vernost'
                    },
                    "url": 'https://apiuat.jetprivilege.com:8082/api/experience/notification/v1/email-notification',
                    "body": reString
                  }, (error, response, body) => {
                    if (error) {
                   //   resolve("failed");
                    } else {
                    //  resolve(response);
                     console.log("resp=========",response.body)
                    }
                  });
                 
                 
  }
  
async function sendEmail(data1, response, status) {
    let tiercode = data1.Tier_Code;
  
    let awb = data1.AWB;
    let Courier = data1.Courier_Partner_Name
    let listid = null;
    if (data1.Courier_Partner_Name == 'FirstFlight') {
      listid = "d-7f6beafb24a24cef8d896258b5810944"//58479552
    }
    else if (data1.Courier_Partner_Name == 'Overnite') {
      listid = "d-2fa8bbeaae7c482cbca35d1bad7346b2"//58498506
    }
    else if (data1.Courier_Partner_Name == 'Indiapost') {
      listid = "d-d91c380f11134ff4ad83526efde5017c"//58482999
    }
    else if (data1.Courier_Partner_Name == 'Trackon') {
      listid = "d-41d4648ea7ba403d999981fb7164370e"//59017867
    }
    let jpno = data1.InterMiles_Membership_Number
    //console.log("Email data===========", awb, tiercode, Courier)
    let temp = JSON.parse(response);
    //console.log("temp data", temp);
    let Title = temp.Title;
    //console.log("Title", Title);
    let Firstname = temp.FirstName;
    //console.log("Firstname", Firstname);
    let Lastname = temp.LastName;
    //console.log("Firstname", Lastname);
    let Email = temp.EmailAddress;
    //  console.log("Email id", Email)

    var json = { "from": { "email": "updates@alerts.intermiles.com","name":"Intermiles" }, "personalizations": [] },
    personalization = {}   
    json["template_id"] = listid;
    personalization["to"] = [
        {
            "email": Email
        }
    ];

    var dynamic_template_data = {};
    dynamic_template_data["Title"] = Title;
    dynamic_template_data["Last_Name"] = Lastname;
    dynamic_template_data["Tier"] = tiercode;
    dynamic_template_data["AWB_NUMBER"] = awb;
    dynamic_template_data["Courier_Vendor_Name"] = Courier;
    dynamic_template_data["JP_NUMBER"] = jpno;
    
    personalization["dynamic_template_data"] = dynamic_template_data;
    json["personalizations"].push(personalization);

    json["prj_code"] = "RTO";
    json["ref_id"] = awb;
    json["sg_key"] = config.sendgrid.api_key;

    request.post({
      "headers": {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + Buffer.from(config.bespoke_simplica.username + ':' + config.bespoke_simplica.password).toString('base64')
      },
      "url": config.sendgrid.url,
      "body": JSON.stringify(json)
    }, (error, response, body) => {
      if (error) {
     //   resolve("failed");
      } else {
      //  resolve(response);
       console.log("resp=========",response.body)
      }
    });
}
  

setInterval(() => {
  sftpconfig();
 // app2.sftpconfig();
},60*180*1000);


